[English](#markdown-header-introduction) | [Español](#markdown-header-introduccion)

#Introducción al laboratorio y a los programados que utilizaremos


![main1.png](images/main1.png)


El curso Introducción a la Programación de Computadoras provee una introducción a los fundamentos de la programación de computadoras y a la solución de problemas mediante el desarrollo de algoritmos, incluyendo técnicas de programación paralela. Los estudiantes dominarán las piezas básicas de un programa comenzando con estructuras de control usando instrucciones condicionales e iterativas con expresiones lógicas, así como la definición y uso de variables y tipos de datos. Desarrollarán procedimientos y funciones para tareas  comunes que se repiten o más complejas que requieren descomposición, incluyendo funciones recursivas. Utilizarán tipos de datos estructurados tales como arreglos, cadenas, archivos y punteros. Además, los estudiantes estarán expuestos a la estructura de la computadora: medios de entrada y salida, memoria y unidad central de procesamiento. Se incluye también una visión general al campo de Ciencia de Cómputos y a su efecto en la sociedad, incluyendo principios éticos en computación.

Durante este semestre los estudiantes del curso de Introducción a la Programación de Computadoras estarán participando del proyecto EIP: "Engaging Introductory Programming Laboratory". En este proyecto, auspiciado por el programa TUES de la National Science Foundation, los estudiantes podrán trabajar en experiencias de laboratorio en donde practicarán los conceptos y destrezas aprendidos en el curso y obtendrán productos atractivos que les ayudarán a entusiasmarse con las posibilidades que ofrece la ciencia de cómputos.

Cada una de las experiencias de laboratorio utilizará bibliotecas y secciones de programas que han sido desarrolladas por profesores y estudiantes del Programa de Bachillerato en Ciencia de Cómputos de la UPR-Río Piedras. Al completar la experiencia de laboratorio el estudiante no solo reforzará los conceptos aprendidos en clase, sino que también conocerá productos que el o ella serán capaces de producir con un poco más de conocimiento y experiencia en cómputos.

En la experiencia de laboratorio del día de hoy aprenderás cómo
trabajaremos en las próximas reuniones, la manera de acceder los
archivos que se usarán y cómo entregar los trabajos. También aprenderás
a manejar los elementos básicos de Qt, la plataforma que nos permitirá
desarrollar y ejecutar proyectos en C++.

##Objetivos:

1. Conocer las metas del laboratorio del curso CCOM 3033.

2. Conocer sobre las experiencias de laboratorio preparadas y las expectativas generales de las sesiones de laboratorio.

3. Conocer la estrategia de programación en parejas.

4. Conocer las reglas de las sesiones de laboratorio y los métodos de evaluación.

5. Practicar el uso de *Git* y *Bitbucket* bajar y guardar los archivos necesarios para cada experiencia de laboratorio.

6. Practicar cómo compilar, corregir errores y ejecutar un programa usando Qt.


---

---

## Sobre el laboratorio

###Política del curso y evaluación

* Todos los estudiantes deberán registrarse en la página de Moodle de la Sección 1 del Laboratorio utilizando un correo electrónico que lean frecuentemente. La página se encuentra en moodle.ccom.uprrp.edu. Toda la información, materiales y asignaciones del curso estará en esta página; cada estudiante es responsable de mirarla regularmente y conocer el calendario de entrega de trabajos.
* Todos los teléfonos celulares deben estar apagados durante la sesión de laboratorio. Si uno de estos aparatos interrumpe la sesión, la profesora asumirá que es una emergencia y se le pedirá al estudiante que salga del laboratorio. El celular tampoco deberá estar en vibración o con señales de luces, ni encima del escritorio. Simplemente apágalo y guárdalo. De esta manera podrás concentrarte en la clase. 
* Solo se utilizarán las laptops del laboratorio.
* La sesión de laboratorio empezará puntualmente y debes llegar a tiempo. En caso de que algo extraordinario ocurra y la profesora tenga que ausentarse o llegar un poco más tarde tratará de informarlo al Departamento. Si llegas más de 15 minutos tarde, por favor, no entres al laboratorio. Si sales del laboratorio, que sea por una emergencia; se espera que una vez empiece la sesión el estudiante permanezca en el laboratorio hasta que el mismo finalice.
* Para cada sesión de laboratorio, habrá documentos en Moodle relacionados a la experiencia de laboratorio que deberás leer antes de llegar al laboratorio. Lee y sigue las instrucciones en la sección *Pre-Lab*. Los documentos de cada experiencia de laboratorio estarán disponibles en Moodle el viernes de la semana anterior.
* Habrá un *Quiz Pre-Lab* que deberás tomar antes de llegar a la sesión de laboratorio. El quiz estará relacionado a los conceptos aprendidos en clase que serán necesarios para la experiencia de laboratorio y también incluirá preguntas de comprobación de lectura de las instrucciones de la experiencia de laboratorio de esa semana. No se repondrán quizes. Se eliminará la peor nota.
* Las experiencias de laboratorio serán trabajadas en pareja. Luego de cada experiencia de laboratorio, cada pareja debe entregar los archivos indicados en las instrucciones usando Moodle. La fecha límite para entregar los archivos de cada experiencia es el viernes siguiente a la sesión de laboratorio. Como las experiencias de laboratorio se trabajarán en pareja, solo se hará una entrega por pareja.
* Al finalizar el trabajo de cada experiencia de laboratorio cada estudiante deberá tomar un *Post-Test*. Con la prueba Pre-Lab y el Post-Test evaluaremos la utilidad de la experiencia de laboratorio en el aprendizaje de conceptos y destrezas. El Post-Test no tendrá puntuación específica en la nota del laboratorio pero será utilizado para dar bonificaciones en la entrega de cada experiencia de laboratorio. 
* Siéntete en libertad de pasar por mi oficina para aclarar cualquier duda. Si la puerta de la oficina está abierta estás bienvenido a entrar; si está cerrada, por favor espera a las horas de oficina. En caso de que tengas conflicto con las horas de oficina asignadas, puedes hacer una cita en cualquier momento.
* Responsabilidad y honestidad son cualidades muy importantes para mí....

####Evaluación 

* La nota de laboratorio será el 25% de la nota de la clase.
* Las pruebas "Pre-Lab" serán el 40% de la nota del laboratorio.
* Las entregas serán el 60% de la nota de laboratorio.
* Los criterios para obtener un bono de 5 puntos en la entrega de cada experiencia de laboratorio son los siguientes:

    1.  Obtienes 80 en el Pre-Lab y mantienes o mejoras la puntuación en Post-Test.
    2.  Obtienes 75 o más en el Post-Test y mejoraste 15 puntos o más de Pre-Lab a Post-Test.
    3.  Obtienes 80 o más en el Post-Test y  mejoraste 10 puntos o más de Pre-Lab a Post-Test.
    4. Obtienes 85 o mas en el Post-Test.
    5. La puntuación total de la entrega no será mayor de 100.




### Programación en parejas

Durante todo el semestre las experiencias de laboratorio se trabajarán usando la técnica de programación en pareja. En cada sesión los estudiantes del curso se dividirán en parejas para trabajar la experiencia de laboratorio. Cada pareja utilizará una computadora y cada estudiante de la pareja tendrá uno de dos roles: conductor o navegante. El conductor estará a cargo de escribir y editar el código o lo que requiera el laboratorio. El navegante estará leyendo y evaluando lo que el conductor escriba para indicar si hubo algún error o hacer sugerencias. La composición de las parejas para cada sesión de laboratorio se anunciará en la página de Moodle. 

### Pre-Lab y Post-Test

Cada experiencia de laboratorio contendrá una sección llamada *Pre-Lab*. En esta sección encontrarás los objetivos de la experiencia y los conceptos que debes repasar o estudiar antes de llegar al laboratorio. En la página de Moodle habrá un enlace a una prueba corta que deberás tomar antes de llegar al laboratorio. Los conceptos que deberás repasar para la experiencia de laboratorio son conceptos cubiertos durante las secciones de conferencia del curso. La prueba corta también tendrá preguntas de comprobación de lectura de las instrucciones del laboratorio.

El *Post-Test* es una prueba corta sobre las mismas destrezas y conceptos que se evaluaron en el Pre-Lab. Esta prueba se toma al concluir la experiencia de laboratorio. Estas pruebas nos permitirán evaluar la efectividad de la experiencia de laboratorio en el aprendizaje de los conceptos y destrezas del curso. Las puntuaciones obtenidas se utilizarán como bonificación en la puntuación del laboratorio.

### Entrega de trabajos

Al finalizar cada experiencia de laboratorio cada pareja deberá entregar archivos con el código implementado o algún otro material. Estas entregas se harán utilizando Moodle y las instrucciones se incluirán en cada experiencia de laboratorio.

---

---

##Servicios y programados que utilizaremos:

### Libro electrónico

Las instrucciones para las experiencias de laboratorio de este semestre están contenidas en el libro electrónico al que puedes acceder en http://ccom.uprrp.edu/~rarce/eipgb/index.html .

En la página de Moodle te indicamos el nombre del laboratorio correspondiente a cada semana y puedes acceder a las instrucciones buscando en el índice del libro electrónico.

Esta es una versión preliminar del libro y por lo tanto puede contener errores. Te agradeceremos que nos indiques si encuentras algún error y si tienes alguna sugerencia que pueda ayudar a que las instrucciones sean más claras.

###Bitbucket y Git


*Bitbucket* es un repositorio o depósito de archivos digitales al que se
puede acceder en línea y que permite trabajar proyectos en grupo de
manera ordenada y simple. Los archivos del Laboratorio de Introducción a
la Programación estarán almacenados en este lugar y se podrán bajar a
las computadoras personales utilizando *Git*.

*Git* es un programado de código abierto que permite manejar archivos de
programas que se desarrollan en grupo. Puedes obtener este programado en
<http://git-scm.com/>. Al comienzo de cada experiencia de laboratorio cargarás los archivos necesarios a una de las laptops del salón usando el comando
`git clone url` desde el terminal de cada computadora. 


###Terminal y Linux


Para utilizar Git en OS X debemos usar comandos de línea en la pantalla de *terminal*. Los comandos que utilizaremos son comandos del sistema operativo *Unix*. Unix distingue entre letras mayúsculas y minúsculas. Algunos comandos básicos de Unix son:

---

|    **Comando**     |                      **Acción**                     |
| ------------------ | --------------------------------------------------- |
| ls                 | muestra lista de los archivos en el directorio      |
| mv nombre1 nombre2 | mueve contenido de nombre1 a archivo nombre2 (cambia nombre del archivo)       |
| cp nombre1 nombre2 | copia contenido de nombre1 a archivo nombre2        |
| rm nombre          | borra archivo                                       |
| mkdir nombre       | crea directorio nombre dentro del directorio actual |
| cd ..              | cambia al directorio anterior                       |
| cd ~               | cambia al directorio hogar                          |
| cd nombre          | cambia al directorio nombre (dentro del actual)     |
| flecha hacia arriba| repite comando anterior                             |

           
**Figura 1.** Comandos básicos de Unix.

---


###Qt


Qt es una aplicación para programación que es utilizada por
desarrolladores que usan el lenguaje de programación C++. Este ambiente
funciona en, y puede crear versiones de, las aplicaciones para distintas
plataformas (desktop, plataformas móbiles y otras). Qt contiene un
ambiente de desarrollo integrado (IDE), llamado *Qt Creator*. Desde ese
ambiente se puede programar y crear interfaces gráficas utilizando la
opción de diseño que contiene Qt. Te instamos a que instales Qt en tu
computadora personal y explores las otras opciones que esta aplicación
provee.

### Máquina virtual

Cada laptop en el laboratorio tiene instalada una *máquina virtual* que contiene la aplicación de Qt necesaria para trabajar las experiencias laboratorios y un terminal en donde podrás escribir los comandos de Unix para bajar los archivos.

Puedes instalar esta máquina en tu computadora personal desde el siguiente lugar: http://eip.ccom.uprrp.edu/vms/eip-ubuntu-qt.tar.gz .

Debes usar esta máquina porque contiene la versión para que los programas para cada experiencia de laboratorio funcionen correctamente. Si instalas otra versión de Qt pudieras tener problemas con algunos de los programas.

---

---

## Usando Qt

Qt es una aplicación usada por desarrolladores profesionales y tiene
muchísimas posibilidades y opciones. En esta experiencia de laboratorio
veremos cómo utilizar las opciones básicas que necesitaremos durante el
semestre.

Para el laboratorio mayormente utilizaremos la parte de editar programas
en C++ pero también existe una opción para diseñar interfaces gráficos.
Esta opción fue utilizada para incorporar el código que presenta los
interfaces gráficos de las experiencias de laboratorio.
El aprender a utilizar esta opción no es parte de este curso pero puedes
aprender a utilizarla por tu cuenta. En https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk/edit puedes encontrar una presentación, preparada por el estudiante Jonathan
Vélez, que muestra aspectos básicos de cómo utilizar la opción de diseño
de interfaces gráficos.

### Proyectos en C++

Cada proyecto en C++ se compone de varios tipos de archivos. En Qt
tendrás archivos del tipo *fuente (source), encabezados (header) y
formulario (form)*.

-   **Archivos "sources":** Estos archivos tienen extensión `.cpp` (C plus
    plus) y contienen el código en C++ de tu programa. Entre estos
    archivos encontrarás el `main.cpp`; este es el archivo que buscará
    el preprocesador y es donde comienza tu programa. Otro de los
    archivos tipo "source" que encontrarás en proyectos creados en Qt es
    el archivo `mainwindow.cpp`; este archivo lo crea Qt y contiene el
    código asociado a la ventana principal diseñada con la opción de
    design (por ejemplo las funciones que aparecen bajo "Private
    slots").

-   **Archivos "headers":** Estos archivos tienen extensión `.h` y
    contienen declaraciones de las funciones que son utilizadas en el programa. Durante el pre-procesamiento de cada programa, la
    instrucción `#include<nombre.h>` incluye el contenido del archivo
    llamado "nombre.h" en el código del archivo `.cpp` que contiene esa
    instrucción.

-   **Archivos "forms":** Estos archivos tienen extensión `.ui` (user
    interface) y contienen los formularios creados con la opción de
    diseño. Un archivo que encontrarás en proyectos creados en Qt es el
    archivo `mainwindow.ui`; este archivo lo crea Qt y contiene el
    diseño de la ventana principal del programa.



---

---


## Sesión de laboratorio

En la experiencia de laboratorio de hoy practicarás el uso de algunos de los programados que utilizarás durante el semestre.

###Ejercicio 0: Crear directorio para los archivos de los laboratorios

Utiliza el terminal y el comando `mkdir` para crear un directorio `Documents/eip` para los archivos de los laboratorios. 


###Ejercicio 1: Comenzar proyecto nuevo

####Instrucciones

1. Para comenzar un proyecto en C++, marca el botón de `New Project` o ve al menú principal de Qt y en `File` selecciona `New File or Project`. Saldrá una ventana similar a la ventana en la Figura 2. Selecciona `Non-Qt Project`, `Plain C++ Project` y marca `Choose`.

    ---

    ![figure2.png](images/figure2.png)

    **Figura 2.** Comenzar proyecto en C++ sin aplicaciones gráficas.

    ---


2. Escribe el nombre del proyecto, selecciona el directorio en donde quieres guardarlo, marca `Continue` en esa y la siguiente ventana y luego `Done` en la próxima.

    Este proceso creará un nuevo proyecto en Qt con el esqueleto de un programa básico en C++ que solo despliega "Hello World!". Antes de continuar, selecciona `Projects` en el menú vertical de la izquierda. Deberá aparecer la pantalla de `Build Settings`. En esa pantalla, asegúrate que la cajita de `Shadow build` NO esté seleccionada, como ocurre en la Figura 3.

    ---

    ![figure3.png](images/figure3.png)

    **Figura 3.** No seleccionar opción `Shadow build`.

    ---



3. Regresa a la pantalla donde puedes editar el programa seleccionando `Edit` en el menú de la izquierda y haciendo "doble click" en `Sources` y luego en `main.cpp`. Presiona la flecha verde en el menú de la izquierda para ejecutar el programa. Los resultados del programa se desplegarán en una pantalla de terminal. Si hubiera errores, estos aparecerán en la pantalla de `Issues` en Qt.

4. Cambia el contenido de `main.cpp` para que sea:

    ```cpp

        #include <iostream>
        using namespace std;


        int main()
        {
            cout << endl << "Me gusta el laboratorio de programacion." << endl;
            return 0;
        }

    ```

5. Marca el botón verde del menú de la izquierda para compilar y ejecutar el programa. Saldrá una ventana que te ofrece la opción de guardar los cambios. Marca `Save all`. Al ejecutar, si no cometiste ningún error, el programa debe desplegar "Me gusta el laboratorio de programacion." en la pantalla de terminal.

###Ejercicio 2: Bajar proyectos de Bitbucket

Los archivos para esta experiencia de laboratorio están guardados en Bitbucket. Para bajar estos archivos a tu computadora ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/introduccion.git`.

###Ejercicio 3: Abrir proyecto ya creado compilar y ejecutar

En este ejercicio practicarás cómo compilar, corregir errores y ejecutar un programa usando Qt. 

####Instrucciones

1. Primero borrarás los archivos creados por Qt y cerrarás los archivos del proyecto anterior. Para hacer esto, en el menú principal de Qt ve a `Build`  y selecciona `Clean all`; luego ve a `File` y selecciona `Close all projects and editors`. 

2. Carga a `QtCreator` el proyecto `Practica`  haciendo doble "click" en  el archivo `Practica.pro` en el directorio `Documents/eip/introduccion` de tu computadora. En la ventana que aparece marca `Configure Project`.

    Cada vez que cargues o comiences algún proyecto asegúrate de que el `Shadow build` no esté seleccionado: en el menú de la izquierda, selecciona `Projects` y luego, en `Build Settings`, verifica que la cajita de `Shadow build` no esté seleccionada, como vimos en la Figura 3.


3. Como viste anteriormente, Qt te permite compilar y ejecutar el programa marcando la flecha verde
que aparece en la columna de la izquierda. Presiona la flecha y nota que
obtienes una ventana de "Issues" que ocurrieron al compilar. La lista
que aparece te muestra información que te permitirá encontrar y corregir
los errores. 

4. Selecciona el archivo `main.cpp` en el directorio de
`Sources` para que puedas encontrar y corregir los errores. 

    Corrige todos los errores y presiona nuevamente la flecha verde para compilar y ejecutar el programa. Una vez
    corrijas todos los errores, el programa debe abrir la pantalla
    `Application Output` y desplegar `Salida: 1`.

5. Como mencionamos antes, durante el proceso de compilación y ejecución Qt crea varios archivos que debemos borrar luego de terminar con el programa. Para hacer esto, en la opción `Build` del menú de Qt, selecciona `Clean All`.


###Ejercicio 4: Entrega de trabajos

Durante cada experiencia de laboratorio cada pareja deberá entregar algunos resultados de su trabajo. Estas entregas se harán en la sección de "Entregas" que aparece en Moodle. Hoy cada estudiante practicará una entrega individualmente. 

####Instrucciones

1. Abre el enlace de "Entregas" en Moodle y entrega el archivo `main.cpp`. Recuerda utilizar buenas prácticas de programación incluyendo  el nombre de los programadores como comentario al inicio de tu programa.


